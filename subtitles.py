import xml.etree.ElementTree as ET
import numpy as np
import json

def convertTime(s):
    hours, minutes, seconds = (["0", "0"] + s.split(":"))[-3:]
    hours = int(hours)
    minutes = int(minutes)
    seconds = float(seconds)
    result = (3600 * hours + 60 * minutes + seconds)
    return result

def unConvertTime(t):
    m,s = divmod(t, 60)
    s,ms = divmod(s,1)
    return "%02d:%02d.%03d" % (m, s, int(1000*ms))
   

ns = '{http://www.w3.org/2006/10/ttaf1}'

# ##
# read the subtitles from the given file between given times
# returns a list of dictionaries with:
#   start -> time of start of word (s from start)
#   end -> time of word end
#   text -> the word itself
#   newVoice -> boolean, if this word is from a new speaker
#   color -> color word was presented in on subtitles
#   tidied=True => duplicates cleared, but colors/new speaker doesn't properly work in this case
def loadSubtitles(filename, clip_start, clip_end, tidied=False):
    tree = ET.parse(filename)
    tt = tree.getroot()
    # print tt
    body = tt.find(ns + 'body/' + ns + 'div')
    plist = body.findall(ns + 'p')
    # print len(plist)
    
    subtitles = []
    lastColor = None

    for sub in plist:
        start_time = convertTime(sub.get("begin"))
        end_time = convertTime(sub.get("end"))
        spans = sub.findall(ns + 'span')
        if len(spans) > 1:
            word = spans[-1]
        else:
            word = spans[0]
        if tidied:
            text = ''
            for w in spans:
                text += w.text
            text = text.strip()
        else:
            text = word.text
        color = word.get('{http://www.w3.org/2006/10/ttaf1#style}color')
        new_speaker = (color != lastColor)
        lastColor = color

        # only collect those in our timespan of interest
        if start_time > clip_start and start_time < clip_end:
            # print start_time,text
            relative_st = start_time - clip_start
            relative_et = end_time - clip_start
            subtitles.append({'start':relative_st,'end':relative_et,'text':text,'newVoice':new_speaker,'color':color})

    return subtitles


# #####
# load subtitle data from json file
def loadSubtitlesFromJson(filename, clip_start, clip_end):
    json_data=open(filename)
    data = json.load(json_data)
    json_data.close()
    
    sts = []
    for st in data:
        if st['beginSecs'] >= clip_start and st['endSecs'] <= clip_end:
            for span in st['spans']:
                # print span.keys()
                begin = st['beginSecs'] - clip_start
                end = st['endSecs'] - clip_start
                text = span['text']
                position = span['position']
                sts.append({'start':begin,'end':end,
                            'text':text,'position':position['bounds']})

    return sts


# #####
# are subtitles visible between t_s and t_e?
# returns proportion of time with subtitles, 
# and text of all that are visible for more than 0.2s
def getSubtitleAt(sts, t_s, t_e):
    answer = 0
    text = ''
    for st in sts:
        if st['start'] < t_s and st['end'] > t_e:
            return 1, st['text']
        elif st['end'] < t_e and st['end'] > t_s:
            # ends within
            time_visible = min([st['end']-t_s,st['end']-st['start']])
            answer += time_visible
            if time_visible > 0.2:
                text += st['text'] + ' '
        elif st['start'] > t_s and st['start'] < t_e:
            # starts within
            time_visible = min([t_e-st['start'],st['end']-st['start']])
            answer += time_visible
            if time_visible > 0.2:
                text += st['text'] + ' '
    return answer/(float(t_e-t_s)), text


