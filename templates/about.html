{% extends "layout.html" %}
{% block title %}About{% endblock %}
{% block body %}
<h1>About this tool</h1>
<p>This is an experimental tool for analysing eye-tracking data.</p>
<h2>Recordings</h2>
<p>This tool is designed for analysing Tobii data files, so the first
thing to do is <a href="{{ url_for('upload_file') }}">upload</a> some.
These should be tsv files and should include at least the following
data fields:</p>

<ul>
<li>RecordingTimestamp</li>
<li>GazePointX (MCSpx)</li>
<li>GazePointY (MCSpx)</li>
<li>MediaName</li>
<li>ParticipantName</li>
<li>GazeEventDuration</li>
</ul>

<p>The order shouldn't matter.  This assumes the data is in the 'new'
format from Tobii - older systems exported tsv files slightly
differently, in which case you need to have the equivalent fields to
those above (the names are slightly different), although participant
name is located in a different position.  Uncheck the 'new format'
checkbox when uploading and it should work.  This has only been tested
on a limited number of files, so it isn't guaranteed that your format
will work.</p>

<h2>Creating Datasets</h2>
<p>A dataset represents a set of recordings, typically one for each of
several participants who looked at the same stimulus.  To create a
dataset, it is first necessary to <a href="{{ url_for('upload_file')
}}">upload</a> one or more recordings.  On the <a href="{{
url_for('recordings') }}">recordings</a> page select <a href="{{
url_for('createDataset') }}">'create a dataset'</a>, then you can
select which portions of which recordings you wish to include and
specify more parameters.  The default behaviour is to use the whole
recording, but it is also possible to select only fixations between
specified times for each recording, or to select those that were made
on a specific stimulus (the name of which must match that recorded in
the raw data file).</p>

<p>The tool also allows a dataset to be divided into different time
slices, e.g., one slice for each 5s.  These may be based on the whole
file, between specified start and finish times, or based on a specific
stimulus.  This allows you to make quick comparisons between different
sections of a recording, e.g., compare the first 10s with the last
10s.</p>

<h3>Parameters and analysis</h3>
<p>Whether a single dataset or a group of slice datasets are created,
you will need to specify the parameters for analysis.  The fixation
grouping radius and the minimum fixation time are used, as described
below, to filter the fixation list, in a similar way to that done by
Tobii Studio.  The error radius and grid dimensions are applied to the
second stage of analysis, where the data is boxed into a grid.</p>

<h4>Filtering</h4>
<p>Fixation filtering is done by this system - consecutive fixations
that are within a set radius are grouped together, then these groups
are filtered to ignore any less than a given length.</p>

<h4>Boxing</h4>
<p>This works as follows:</p>

<ol>
  <li>For each participant, all the fixations are collected to create a
pixel map of the screen.</li>
  <li>This is then smoothed, using the smoothing radius, to account
for potential error in the measurement of the position of each
fixation.</li>
  <li>This smoothed pixel map is then divided into a grid, with each
box having the number of fixations that fell on it, the mean duration
of those fixations, and the total time the box was attended.  Time to
first fixation is also calculated for each box, using un-smoothed
data.</li>
  <li>Finally the number of transitions between each possible pair of
boxes is calculated, also on the unsmoothed fixation.</li>
  <li>Once this has been done for each participant, average values are
  created for each box for the dataset as a whole.</li>
</ol>

<p>The grid size can be modified later: click the 'explore' link next
to the grid size where the parameters are displayed - this will show
you plots for the total attention on the dataset over a series of grid
sizes. The size shown in bold is that which is thought to be optimum,
based on the data.  Clicking apply on any grid will re-box the dataset
to that grid size.</p>

<h3>Videos and background images</h3>
<p>You may provide a background image that the plots will be overlaid
on - this should have the same aspect ratio as the screen.  If this is
not provided, plots will be presented with a white background.</p>

<p>This tool will also play a video showing the individual, filtered
but unboxed, fixations.  If the stimulus was a video, this may be
uploaded (in <i>webm</i> format), and the fixations will be overlaid
on it.  An offset optionally allows you to specify the time in the
video at which the recordings started, the default is 0 (the start of
the video).  The video height is the height of the video, e.g., if a
widescreen video did not fill the screen that was eye-tracked.  If
left empty, it is assumed that the video took the full screen.  If
multiple datasets are created with the same video stimulus, you only
need to upload the video once and it may be re-used.  If no video is
provided, fixations will be played over the background image, if it
exists, else a blank background.</p>


<h2>Viewing Datasets</h2>
<p>Once a <a href="{{ url_for('all_datasets') }}">dataset</a> has been
created you can view it!  The view shows the recordings, with links to
a plot of the fixations for that participant (and from there a further
link takes you to a page with fixation plots for all participants.
There are also plots showing the distribution of attention over the
grid, in terms of: number of fixations; total fixation time; mean
fixation duration; fixation frequency (fixations per second), and;
time to first fixation.  Finally there is a plot showing the
transitions between boxes - arrow size representing transition
frequency.  Clicking on any of these (except the paths) will take you
to a page with a larger plot and a table of data for each box.
Clicking on any box in the data table will reveal further data in the
form of a table showing the results for that box for each participant,
as well as summary statistics.</p>

<h2>Comparing Datasets</h2>
<p>Two datasets can be compared if they have been analysed with the
same parameters.  The first view of the comparison shows a plot of the
fixations for each dataset (all participants).  Below this are links
to comparisons of the various results (fixation count, etc.).  These
pages give a plot showing where the datasets varied, using a circle at
each box position.  The size of the circle indicates the magnitude of
the difference and the colour (green/blue) the direction.  The shade
of colour indicates the p value for the significance test (darker
means more significant); differences where <i>p < 0.1</i> are highlighted with
an orange dot in the centre, while those where <i>p < 0.05</i> are highlighted
with a red dot.  Again, a table below the plot gives numeric data,
with each box having the mean value for each dataset and the p value.
Clicking on a box reveals the data for that box, with the values for
each participant in each dataset, as well as the means, medians and
standard deviations of the two sets.</p>

<h2>Further information</h2>
<p>This tool has been developed by <a href='mailto:andrew.brown@cs.manchester.ac.uk'>Andy Brown</a> in the <a href='http://wel.cs.manchester.ac.uk'>Web Ergonomics Lab</a> at the University of Manchester.  The code is available on <a href='https://bitbucket.org/andyjbrown/eye-tracking-analysis'>Bitbucket</a>; it is liable to change!</p>
<p>The video visualisation tool is an adaptation of one developed by
Matt Brooks at BBC R&amp;D.</p>

{% endblock %}
