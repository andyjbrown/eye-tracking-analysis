import numpy
import os
import scipy.stats.stats as st  
import scipy.stats as stats
import matplotlib
from matplotlib.collections import PatchCollection
import matplotlib.cm as cm

# colormap for stats plots
stats_colormap = {'green':  ((0.0, 0.0, 0.0),
                     (0.25,0.0, 0.0),
                     (0.5, 0.8, 1.0),
                     (0.75,1.0, 1.0),
                     (1.0, 0.4, 1.0)),
          
          'red': ((0.0, 0.0, 0.0),
                  (0.25,0.0, 0.0),
                  (0.5, 0.8, 0.8),
                  (0.75,0.0, 0.0),
                  (1.0, 0.0, 0.0)),
          
          'blue':  ((0.0, 0.0, 0.4),
                    (0.25,1.0, 1.0),
                    (0.5, 1.0, 0.8),
                    (0.75,0.0, 0.0),
                    (1.0, 0.0, 0.0)),
          
          'alpha': ((0.0, 1.0, 1.0),
                    #(0.4, 0.2, 0.2),
                    (0.5, 0.3, 0.3),
                    #(0.6, 0.2, 0.2),
                    (1.0, 1.0, 1.0))
          }
       

from pylab import *

    # ######################################################
    # plotting functions
    # ######################################################


class Plotter:

    def __init__(self, gridx, gridy, outputPath, screen):
        self.gridx = gridx
        self.gridy = gridy
        self.outputPath = outputPath
        self.screen = screen
        

    # #####
    # graph results using gnuplot
    # returns the full path to the image file 
    def plotDataSet(self, ds, plot, zmax=0.6, label=None, outfile=None, image=None):
        if not outfile:
             outfile = os.path.join(self.outputPath, ds.label + ".png")
             if not label:
                 label = ds.label

        aggregateData, offScreen = ds.getAggregateData()
        # print offScreen
        plotData = self.generatePlotArray(aggregateData, plot)#, len(ds.dataFiles))
        self.writePlot(plotData, label, zmax, filename=outfile, image=image)
        return outfile


    # #####
    # generate data in format for matplotlib to read, 
    # i.e, a numpy array
    def generatePlotArray(self, boxArray, plot, sampleSize=0):
        resultArray = numpy.empty((self.gridy, self.gridx))
        resultArray[:] = numpy.nan
        for i in range(self.gridx):
            for j in range(self.gridy):
                box = boxArray[i][j]
                count = box.count
                try:
                    count = sum(count)
                except:
                    print "count exception"
                    pass
                if count > sampleSize:  # only plot if averaged at least one fixation per participant
                    z = st.nanmean(box.getResult(plot))
                    resultArray[j][i] = z
        return resultArray

    # #####
    # create a plot using matplotlib
    # expects data as [[x,y,val], [x,y,val],...]
    def writePlot(self, myData, plot_title, zMax, filename=None, image=None):
        print "Plotting to",filename
        fig = figure()

        if image is not None:
            try:
                im = imread(image)
                imshow(im, alpha=0.5, extent=[-0.5,self.gridx-0.5,self.gridy-0.5,-0.5])
            except IOError:
                print "no image " + image

        imshow(myData, interpolation='nearest', alpha=0.7)
        colorbar(shrink=0.5,aspect=10) # adds scale for colour map, shrunk slightly
        grid(True)
        title(plot_title) # adds title
        
        ylim(self.gridy-0.5, -0.5) # set axis limits - puts plot right way up
        xlim(-0.5, self.gridx-.5)
        if zMax is not None:
            clim(0, zMax) # set color range scale

        # draw a rectangle for the bbc image
        #picture_base = ( (152.0/1024) * self.gridy ) - 0.5
        #picture_top = ( (872.0/1024) * self.gridy ) - 0.5
        #axhspan(picture_base, picture_top, facecolor='0.5', alpha=0.3) # draw a rectangle on the plot
               
        ax = axes() # now hide the axis scales
        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().set_visible(False)
        # ax.set_aspect(self.screen['width']/float(self.screen['height']))

        # save to file
        if filename is None:
            return fig, ax
        else:
            savefig(filename)
            clf()
            close(fig)

    # #####
    # plot the stats comparison using matplotlib
    # plots circle per box: 
    #    size represents significance of difference
    #    shading represents magnitude of difference
    #    also plots small and large red circles on points where p < 0.1 and p < 0.05
    #
    # stats in form: {'x': x coords, 'y': y coords: 's' size, 'p': pval }
    # size represents size and direction of the difference
    # generate using Analysis.generateMplotStats()
    def plotComparisonStats(self, data, plot, plot_title, filename=None):

        filepath = os.path.join(self.outputPath, filename)

        # first extract arrays to plot 90 and 95% confidence markers
        p95s = ([],[]) # ([xvalues][yvalues])
        p90s = ([],[])
        for i in range(len(data['p'])):
            if math.fabs(data['p'][i]) >= 0.95:
                p95s[0].append(data['x'][i])
                p95s[1].append(data['y'][i])
            elif math.fabs(data['p'][i]) >= 0.9:
                p90s[0].append(data['x'][i])
                p90s[1].append(data['y'][i])

        register_cmap(name='BlueRedAlpha', data=stats_colormap)
        
        # now do the plotting
        fig = figure()

        # set axis limits - puts plot right way up
        ylim(self.gridy-0.5, -0.5) 
        xlim(-0.5, self.gridx-0.5)

        # plot main data
        scatter(data['x'],data['y'],s=data['s'], c=data['p'])
        set_cmap('BlueRedAlpha')
        # colorbar(shrink=0.5,aspect=10) # adds scale for colour map, shrunk slightly
        
        # plot 90 and 95% confidence markers
        scatter(p90s[0],p90s[1],s=20, c='orange')
        scatter(p95s[0],p95s[1],s=40, c='red')

        legend_handles = [Rectangle((0,0), 1, 1, fc='g'),Rectangle((0,0), 1, 1, fc='b')]
        legend(legend_handles, ['higher','lower'], prop={'size':8})
        title(plot_title) # adds title
        ax = axes() # now hide the axis scales
        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().set_visible(False)
        # axes().set_aspect(self.screen['height']/float(self.screen['height']))

        if filename is None:
            return fig, ax
        else:
            savefig(filepath)
            clf()
            close(fig)
            return filename


    # #####
    # plot fixations from one or more participants
    def plotFixations(self, participants, filename=None, image=None):
        # alpha depends on number of participants
        if len(participants) > 1:
            alpha = 0.2
        else:
            alpha = 0.5
        
        fig = figure()
        ax = axes()
        
        if image is not None:
            try:
                im = imread(image)
                imshow(im, alpha=0.5, extent=[0,self.screen['width'],self.screen['height'],0])
            except IOError:
                print "no image"

        legend_handles = []
        legend_labels = [p.number for p in participants]

        color_cycle = iter(cm.jet(np.linspace(0, 1, len(participants))))
        for p in participants:
            fixations = p.fixationList
            # alpha also depends on number of participants
            if len(fixations) > 100:
                alpha = 0.2

            circles = []
            for fix in fixations:
                circles.append(Circle((fix.x,fix.y), fix.duration/float(15),alpha=0.1))
            pCol = next(color_cycle)
            p = PatchCollection(circles, alpha=alpha, color=pCol)
            legend_handles.append(scatter(range(1), range(1), color=pCol, alpha=0.3))
            ax.add_collection(p)    

        legend(legend_handles, legend_labels, numpoints=1, scatterpoints=1, markerscale=3)

        xlim(0,self.screen['width']) 
        ylim(self.screen['height'], 0)
        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().set_visible(False)
        ax.set_aspect(self.screen['width']/self.screen['height'])

        if filename is None:
            return fig, ax
        else:
            savefig(filename)
            clf()
            close(fig)

    # #####
    # overlay plots of fixations from two datasets 
    # dsA plotted in green, dsB in blue
    # saved to filename, if None returns fig,ax
    # optionally add overlay - this is a PatchCollection
    def plotFixationComparison(self, dsA, dsB, filename=None, overlay=None):
        print "Plotting fixation comparison to",filename
        fig, ax = subplots()
        legend_handles = [scatter(range(1), range(1), color='g', alpha=0.3),  scatter(range(1), range(1), color='b', alpha=0.3)]
        legend_labels = [dsA.label, dsB.label]

        for p in dsA.participantList:
            fixations = p.fixationList
            circles = []
            for fix in fixations:
                circles.append(Circle((fix.x,fix.y), fix.duration/float(10),alpha=0.1))
            p = PatchCollection(circles, alpha=0.2, color='g')
            ax.add_collection(p)    

        for p in dsB.participantList:
            fixations = p.fixationList
            circles = []
            for fix in fixations:
                circles.append(Circle((fix.x,fix.y), fix.duration/float(10),alpha=0.1))
            p = PatchCollection(circles, alpha=0.1, color='b')
            ax.add_collection(p)    

        legend(legend_handles, legend_labels, numpoints=1, scatterpoints=1, markerscale=3)

        if overlay is not None:
            ax.add_collection(overlay)

        xlim(0,self.screen['width']) 
        ylim(self.screen['height'], 0)
        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().set_visible(False)
        axes().set_aspect(self.screen['width']/self.screen['height'])

        if filename is None:
            return fig, ax
        else:
            savefig(os.path.join(self.outputPath,filename))
            clf()
            close(fig)
        

    # #####
    # generate a plot showing the gaze paths between boxes for a dataset
    def plotPaths(self, dataset, filename=None, image=None):
        gridx = self.gridx
        gridy = self.gridy
        numBoxes = gridx * gridy

        # collect data into matrix
        paths = dataset.participantList[0].generatePathData(gridx, gridy)
        mat = matrix(paths)
        for p in dataset.participantList[1:]:
            mat += matrix(p.generatePathData(gridx, gridy))

        # find max (between boxes) to scale with
        maxval = 0
        for i in range(numBoxes):
            for j in range(numBoxes):
                if i != j:
                    if mat[i,j] > maxval:
                        maxval = mat[i,j]
        print "Max = " + str(maxval)
        
        # plot
        fig = figure()

        if image is not None:
            try:
                im = imread(image)
                imshow(im, alpha=0.5, extent=[-0.5,self.gridx-0.5,self.gridy-0.5,-0.5])
            except IOError:
                print "no image"

        ylim(self.gridy-0.5, -0.5) 
        xlim(-0.5, self.gridx-0.5)
        same = [[],[],[]]
        # plot an arrow for each path
        ax = axes()
        for i in range(numBoxes):
            for j in range(numBoxes):
                xa = i%gridx
                ya = i/gridx
                xb = j%gridx
                yb = j/gridx
                val = mat[i,j]/maxval
                if xa == xb and ya == yb:
                    same[0].append(xa)
                    same[1].append(ya)
                    same[2].append(val*150)
                elif val > 0.1:
                    ax.arrow(xa, ya, xb-xa, yb-ya, head_width=0.5*val, alpha=(val), width=0.2*val, ec='k', length_includes_head=True)

        # plot within box paths
        scatter(same[0],same[1],s=same[2],alpha=0.4)

        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().set_visible(False)
        axes().set_aspect(self.screen['width']/self.screen['height'])

        if filename is None:
            return fig, ax
        else:
            savefig(filename)
            clf()
            close(fig)
