# ####################################################
# locus_plot.py
#
# ###############
#
# creates visualisations showing how attention 
# varies over time
#
# mainly displaying most fixated box within a time slice
# 

import numpy as np
import scipy.stats as stats
import scipy.stats.stats as st  
import analysis
import math
import matplotlib.pyplot as plt
import operator
import os, sys
from matplotlib._png import read_png
from pylab import ogrid
from matplotlib.patches import Rectangle

import time_slicing as ts

# ##############################################################################
# the plotting functions
# ##############################################################################


# ##
# get the variation in mean attention for a box over time
def plotBoxOverTime(dataset, (number_slices, slice_length), boxid):
    participants = ds.participantList
    out = [] 
    for data_slice in range(0, number_slices):
        (mean, std, count) = getBoxDataForTime(slice_length, data_slice, participants, boxid)
        print data_slice,(mean, std, count)
        out.append((mean, std, count))

    fig = plt.figure(figsize=(12.0, 8.0))
    x_axis = [((r*slice_length + float(slice_length)/2)/1000) for r in range(0, number_slices)]
    ys = [s[0] for s in out]
    stdevs =[s[1] for s in out]
    counts = [s[2] for s in out]
    plt.errorbar(x_axis, ys, yerr=stdevs)
    # plt.plot(x_axis, counts)
    plt.show()
    # return out
        

# ##
# get the variation in attention over time (how much attention is 
# distributed over scene
def plotDistOverTime(dataset, (number_slices, slice_length), grid, radius):
    participants = ds.participantList
    out = [] 
    for data_slice in range(0, number_slices):
        boxes = ts.getBoxesForTime(slice_length, data_slice, participants, grid, radius)
        boxes = ts.sortBoxes(boxes, grid)
        count = len([b for b in boxes if b[1] > 0])
        sd = np.std([b[1] for b in boxes])
        # print 'count',count
        out.append((count, sd))

    fig = plt.figure(figsize=(12.0, 8.0))
    x_axis = [((r*slice_length + float(slice_length)/2)/1000) for r in range(0, number_slices)]
    counts = [s[0] for s in out]
    stdevs =[10*len(participants)*s[1] for s in out] # bigger => more concentrated

    # plt.title(f_name)
    plt.plot(x_axis, counts)
    plt.plot(x_axis, stdevs)
    plt.show()
    # return out
        

##
# writes manifest and data files to display main focus using Matt Brooks subseyevis tool
#   manifest path is full path to dir where manifest goes
#   dataFilename is full path and filename for json file of data
#   vid filename is path (relative to subseyevis) and filename of video
#   videoHeight is the height of the video as it appeared on screen, in pixels
def writeVisualiserFiles(data, slice_params, vidFileName, dataFilename, gridDimensions, (screenW, screenH), videoHeight, aw=False, manifestPath=None,videoOffsetTime=0):
    num_slices, slice_l = slice_params
    import json
    startTime = 0 # -1000
    endTime = startTime + (num_slices * slice_l)
    # print "Saving " + filename + " with offset=" + str(videoOffsetTime) 
    # assume video took full width
    
    segmenttime = float(endTime - startTime)/1000
    header = { "ParticipantName": "Modal position",
               "RecordingName": "",
               "MediaWidth": screenW,
               "MediaHeight": screenH,
               "GridWidth": gridDimensions[0],
               "GridHeight": gridDimensions[1],
               "SegmentName": "",
               "SegmentDuration": segmenttime, 
               "Type": "locus", 
               "LocusBox": {"w": gridDimensions[2]/float(screenW), "h":gridDimensions[3]/float(screenH)} }
    timestamps = []
    durations = []
    locations = []
    count = 0 # specific to autumnwatch
    if aw:
        tv, ipad = exp1.getAttentionOverAll(slice_l) # specific to autumnwatch
    for data_slice in data:
        x = data_slice[0]
        y = data_slice[1]
        time = data_slice[3]
        duration = data_slice[2]
        if aw:
            ipad_prop = ipad[count] # specific to autumnwatch
        xpos = float(x)/screenW
        ypos = float(y-(float(screenH-videoHeight)/2))/videoHeight
        if duration > 0:
            timestamps.append(float(time-startTime+videoOffsetTime)/1000)
            durations.append(slice_l)
            if aw:
                locations.append({"x": xpos, "y": ypos, "alpha": duration, "prop": ipad_prop})
            else:
                locations.append({"x": xpos, "y": ypos, "alpha": duration})
        else:
            print 'no attention on slice',time
        count += 1

    all = { "header": header,
            "RecordingTimestamp": timestamps,
            "GazeEventDuration": durations,
            "FixationPoint": locations
    }

    outFile = open(dataFilename, 'w')
    outFile.write(json.dumps(all))
    outFile.close()


    if manifestPath is not None:
        # create manifest file
        manifest =  [{
            "subtitleFilename": "",
            "tradSubtitleFilename": "",
            "clipName": "video",
            "clipNameShort": "Locus tester",
            "clipLanguage": "English",
            "subtitleOffset": 0.0,
            "trackData": [
                {
                    "treatment": 1,
                    "videoFilename": vidFileName,
                    "files": [dataFilename]
                }]
        }] 
    
        all = { "trackDataPath":["."],
                "trackVisTypes":["Normal"] ,
                "trackVisTypesShort":["NML"],
                "subtitleControl":["None"],
                "recalibration":{ },
                "manifest":manifest
            }
    
        outFile = open(manifestPath + "manifest.json", 'w')
        outFile.write(json.dumps(all))
        outFile.close()


##
# output video with most popular box
#   if detailed=True, all attention indicated by opacity
def outputVideo(dataset, slice_params, outfileName, grid, screen, radius, bg_image=None, overlay=None, detailed=False, path='./', video=True, keepStatic=False):
    print 'Generating video...'
    num_slices, slice_l = slice_params
    gridWidth, gridHeight, boxWidth, boxHeight = grid
    participants = dataset.participantList
    screenW, screenH = screen

    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111)
    i = 0
    for data_slice in range(0, num_slices):
        # get boxes
        boxes = ts.getBoxesForTime(slice_l, data_slice, participants, grid, radius)
        boxes = ts.sortBoxes(boxes, grid)

        ax.cla()
        # ax.text(100, -20, str(data_slice*slice_l))
        # ax.set_aspect('equal', 'datalim')
        
        # show image
        if bg_image is not None:
            if isinstance(bg_image, list):
                img_f = bg_image[i]
            else:
                img_f = bg_image
            try:
                img = read_png(img_f)
                plt.imshow(img, extent=[0,screenW,screenH,0])
            except IOError:
                print 'could not read ',img_f

         # plot boxes
        if detailed:
            for box in boxes:
                p1 = Rectangle((box[0][0]-boxWidth/2, box[0][1]-boxHeight/2), boxWidth, boxHeight, fc="black", alpha=1-(box[1]**0.15))
                ax.add_artist(p1)

        # highlight mode box
        modebox = boxes[0]
        p1 = Rectangle((modebox[0][0]-boxWidth/2, modebox[0][1]-boxHeight/2), boxWidth, boxHeight, ec="r", fill=None)
        ax.add_artist(p1)
        
        # add overlays
        if overlay is not None:
            if isinstance(overlay, list):
                ol = overlay[i]
            else:
                ol = overlay
            for o in ol:
                # print o
                ax.add_artist(o)

        # set plot details
        plt.xlim(0,screenW)
        plt.ylim(screenH,0)
        ax.grid(False)
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        fname = '%s_slice%03d.png'%(outfileName,i)
        fig.savefig(os.path.join(path,fname), bbox_inches='tight', transparent=True)
        i += 1
    plt.clf()
    plt.close(fig)

    # generate video
    if video:
        framerate = float(1000)/(2*slice_l) # run at half actual speed
        print 'Making movie animation.mpg - this make take a while'
        os.system("mencoder 'mf://' + outfileName + '_*.png' -mf type=png:fps=" + str(framerate) + " -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o " + outfileName + ".mpg")

    # remove image files
    if not keepStatic:
        # print 'deleting images: rm -f %s%s_slice*.png'%(path,outfileName)
        os.system("rm -f %s%s_slice*.png"%(path,outfileName))

    print ' complete'
    return
    

# ## 
# plot it and display on screen
def plotData(data, slice_params, (screenW, screenH), imageFile=None, twoD=True, threeD=True, saveFile=None):
    num_slices, slice_l = slice_params
    # and a list of zeros, to allow us to plot on walls of 3D plot
    zeros = [0 for r in range(0, len(data))]

    # x and y on separate graphs
    x_axis = [data_slice[3]/1000 for data_slice in data]
    x_modes = [data_slice[0] for data_slice in data]
    y_modes = [data_slice[1] for data_slice in data]
    counts = [data_slice[2]*2000 for data_slice in data]

    if twoD:
        fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True, figsize=(12.0, 8.0))
        plt.xlim(0, slice_l*num_slices/1000)
        ax1.scatter(x_axis, x_modes, facecolor='r', s=[c/5 for c in counts])
        ax1.plot(x_axis, x_modes, color='r', marker='')
        ax2.plot(x_axis, y_modes, color='r', marker='')
        ax2.scatter(x_axis, y_modes, facecolor='r', s=[c/5 for c in counts])
        ax1.set_ylim(0,screenW)
        ax2.set_ylim(0,screenH)

    # 3d plot
    if threeD:
        from mpl_toolkits.mplot3d import axes3d
        fig = plt.figure(figsize=(12.0, 8.0))
        ax3 = fig.gca(projection='3d')
        ax3.plot3D(x_modes, x_axis, y_modes, color="#003355", marker='')
        ax3.scatter3D(x_modes, x_axis, y_modes, s=counts)
        # show scale of 100% attention:
        ax3.scatter3D(x_modes, x_axis, y_modes, facecolor='', edgecolor="#cccccc",s=2000)
        
        # display image on x,z plane (slow)
        if imageFile is not None:
            # print width, height
            img = read_png(imageFile)
            # im_y, im_x = ogrid[0:height, 0:width]
            x, y = ogrid[0:img.shape[0], 0:img.shape[1]]
            ax3.plot_surface(x, y, 10, rstride=5, cstride=5, facecolors=img)
            # ax3.plot_surface(im_y, x_axis[-1], height-im_x, rstride=5, cstride=5, facecolors=img, alpha=0.3)
    
        # project onto planes
        ax3.plot3D(zeros, x_axis, y_modes, color="#003355", marker='o', alpha=0.2)
        ax3.plot3D(x_modes, x_axis, zeros, color="#003355", marker='o', alpha=0.2)
    
        ax3.set_ylabel('time (s)')
        ax3.set_xlim3d(0,screenW)
        ax3.set_zlim3d(0,screenH)

    # plt.show()
    if saveFile is None:
        plt.show()
    else:
        fig.savefig(saveFile + "-locus.png", bbox_inches='tight')
  
 
# ##
#  test diff function - returns a list if tuples
# each item in list corresponds to one slice in the data
# each tuple is 2 measures of the difference between that slice
# in the two datasets
def diffDatasets(dataset, ds2, (number_slices, slice_length), grid, radius, plot=None):
    participants = dataset.participantList
    sliceArray = [] 
    for data_slice in range(0, number_slices):
        all_boxes = ts.getBoxesForTime(slice_length, data_slice, participants, grid, radius)
        # all_boxes = sortBoxes(all_boxes, grid)
        sliceArray.append(all_boxes)

    participants2 = ds2.participantList
    sliceArray2 = [] 
    for data_slice in range(0, number_slices):
        all_boxes = ts.getBoxesForTime(slice_length, data_slice, participants2, grid, radius)
        # all_boxes = sortBoxes(all_boxes, grid)
        sliceArray2.append(all_boxes)

    cogs1 = ts.getSliceCogData((number_slices, slice_length), participants)
    cogs2 = ts.getSliceCogData((number_slices, slice_length), participants2)
    cogdiffs = []
    for i in range(len(cogs1)):
        dx = cogs2[i][0] - cogs1[i][0]
        dy = cogs2[i][1] - cogs1[i][1]
        dist = math.sqrt(dx**2 + dy**2)
        cogdiffs.append(dist)

    diffs = []
    for sliceid in range(len(sliceArray)):
        sA = sliceArray[sliceid]
        sB = sliceArray2[sliceid]
        if plot is not None:
            path, prefix = plot
            plotSliceDiff(sA,sB,grid, path, '%s-diff_slice%03d.png'%(prefix,sliceid))
        diffvals = ts.gridDiff(sA, sB, grid, smooth=None)
        diffs.append(diffvals)

    results = {}
    for k in diffs[0].keys():
        results[k] = [r[k] for r in diffs]
    results['cog'] = cogdiffs
    return results

# ###
# generate a plot showing the difference between slices
def plotSliceDiff(sliceA, sliceB, grid, (screenW, screenH), path, fname):
    grid_i, grid_j, boxWidth, boxHeight = grid
    num_boxes = grid[0]*grid[1]
    diff = [sliceA[boxid]-sliceB[boxid] for boxid in range(num_boxes)]
    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111)
    for boxid in range(num_boxes):
        x,y = ts.getCenterCoords(boxid, grid)
        delta = diff[boxid]
        if delta < 0:
            p1 = Rectangle((x-boxWidth/2, y-boxHeight/2), boxWidth, boxHeight, fc="red", alpha=(-delta))
        else:
            p1 = Rectangle((x-boxWidth/2, y-boxHeight/2), boxWidth, boxHeight, fc="green", alpha=(delta))
            
        ax.add_artist(p1)
    plt.xlim(0,screenW)
    plt.ylim(screenH,0)
    fig.savefig(os.path.join(path,fname), bbox_inches='tight')
    plt.clf()
    plt.close(fig)


